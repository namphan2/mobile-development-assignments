package com.npdevelopment.shoppinglist;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class ShoppingListActivity extends AppCompatActivity implements RecyclerView.OnItemTouchListener {

    private RecyclerView rvShoppingList;
    private Snackbar mSnackBar;
    private Context context;
    private TextInputEditText mInputEditText;
    private ShoppingListAdapter mShoppingListAdapter;
    private List<Product> shoppingList = new ArrayList<>();

    private ProductRoomDatabase db;
    private Executor executor = Executors.newSingleThreadExecutor();
    private GestureDetector mGestureDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_list);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mInputEditText = findViewById(R.id.add_item_input);
        rvShoppingList = findViewById(R.id.product_recyclerview);

        db = ProductRoomDatabase.getDatabase(this);

        FloatingActionButton fab = findViewById(R.id.add_item_fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Check if input is empty
                if (!(TextUtils.isEmpty(mInputEditText.getText().toString()))) {
                    String newItem = mInputEditText.getText().toString();
                    insertProduct(new Product(newItem));

                    mSnackBar = Snackbar.make(view, getString(R.string.success_message), Snackbar.LENGTH_SHORT);
                    View sbView = mSnackBar.getView();
                    sbView.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                    mSnackBar.show();
                } else {
                    //Show a message to the user if the textfield is empty
                    mSnackBar = Snackbar.make(view, getString(R.string.fields_required), Snackbar.LENGTH_SHORT);
                    View sbView = mSnackBar.getView();
                    sbView.setBackgroundColor(getResources().getColor(R.color.red));
                    mSnackBar.show();
                }
            }
        });

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        rvShoppingList.setLayoutManager(mLayoutManager);
        rvShoppingList.setHasFixedSize(true);
        rvShoppingList.addOnItemTouchListener(this);
        rvShoppingList.addItemDecoration(new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL));

        mShoppingListAdapter = new ShoppingListAdapter(shoppingList);
        rvShoppingList.setAdapter(mShoppingListAdapter);
        getAllProducts();

        mGestureDetector = new GestureDetector(this, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public void onLongPress(MotionEvent e) {
                super.onLongPress(e);
                View child = rvShoppingList.findChildViewUnder(e.getX(), e.getY());

                if (child != null) {
                    int adapterPosition = rvShoppingList.getChildAdapterPosition(child);
                    deleteProduct(shoppingList.get(adapterPosition));
                }
            }
        });
    }

    /**
     * Update the shopping list
     */
    private void updateUI(List<Product> products) {
        shoppingList.clear();
        shoppingList.addAll(products);
        mShoppingListAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_shopping_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_delete_item) {
            // Deleting all items and notifying our list adapter of the changes.
            deleteAllProducts(shoppingList);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onInterceptTouchEvent(@NonNull RecyclerView recyclerView, @NonNull MotionEvent motionEvent) {
        mGestureDetector.onTouchEvent(motionEvent);
        return false;

    }

    @Override
    public void onTouchEvent(@NonNull RecyclerView recyclerView, @NonNull MotionEvent motionEvent) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean b) {

    }

    /**
     * Get all products from the database and update the uit with these products.
     */
    private void getAllProducts() {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                final List<Product> products = db.productDao().getAllProducts();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        updateUI(products);
                    }
                });
            }
        });
    }

    private void insertProduct(final Product product) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                db.productDao().insert(product);
                getAllProducts();
                runOnUiThread(new Runnable() { // Optionally clear the text from the input field
                    @Override
                    public void run() {
                        mInputEditText.setText("");
                    }
                });
            }
        });
    }

    private void deleteProduct(final Product product) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                db.productDao().delete(product);
                getAllProducts();
            }
        });
    }

    private void deleteAllProducts(final List<Product> products) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                db.productDao().delete(products);
                getAllProducts();
            }
        });
    }
}
