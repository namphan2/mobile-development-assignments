package com.npdevelopment.hvaquest;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class QuestionActivity extends AppCompatActivity {

    private TextView questionText;
    private RadioButton radioButtonA, radioButtonB, radioButtonC, answer;
    private Button checkAnswerBtn;
    private RadioGroup radioGroupAnswers;

    private String[] items;
    private String correctAnswer;
    private int questionNumber = 1;
    public static final String questionKey = "questionKey";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);

        questionText = findViewById(R.id.questionText);
        radioGroupAnswers = findViewById(R.id.radioGroupAnswers);
        radioButtonA = findViewById(R.id.radioBtnA);
        radioButtonB = findViewById(R.id.radioBtnB);
        radioButtonC = findViewById(R.id.radioBtnC);
        checkAnswerBtn = findViewById(R.id.checkAnswerBtn);

        populateQuestion(questionNumber);

        checkAnswerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectedId = radioGroupAnswers.getCheckedRadioButtonId();
                if (selectedId == -1) {
                    Snackbar.make(v, getApplicationContext().getString(R.string.choose_answer), Snackbar.LENGTH_LONG).setAction("Action", null).show();
                } else {
                    answer = findViewById(selectedId);

                    if (answer.getText().equals(correctAnswer)) {
                        Intent intent = new Intent(QuestionActivity.this, LocationActivity.class);
                        intent.putExtra(questionKey, questionNumber);
                        startActivityForResult(intent, 1234);
                    } else {
                        Snackbar.make(v, getApplicationContext().getString(R.string.try_again), Snackbar.LENGTH_LONG).setAction("Action", null).show();
                    }
                }
            }
        });
    }

    public void populateQuestion(int questionNumber) {
        //   populate question
        String question = "question" + questionNumber;
        int holderint1 = getResources().getIdentifier(question, "string",
                this.getPackageName()); // You had used "name"

        String questionScreen = getResources().getString(holderint1);
        questionText.setText(questionScreen);

        // populate answer
        String answer = "answer" + questionNumber;
        int holderint = getResources().getIdentifier(answer, "array",
                this.getPackageName()); // You had used "name"

        items = getResources().getStringArray(holderint);

        radioButtonA.setText(items[1]);
        radioButtonB.setText(items[2]);
        radioButtonC.setText(items[3]);
        correctAnswer = items[0];
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //Check if the result code is the right one
        if (resultCode == Activity.RESULT_OK) {
            //Check if the request code is correct
            if (requestCode == 1234) {
                questionNumber = data.getIntExtra(LocationActivity.clueKey,-1);
                populateQuestion(questionNumber);
                radioGroupAnswers.clearCheck();
            }
        }
    }
}
